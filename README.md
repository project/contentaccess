## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Notes
* Maintainer


## INTRODUCTION

The **ContentAccess** module let you content manage permissions to
view a published node.

It provides two new permissions: *view all* (allows anyone to view the
node) and *view own* (allows only the author to see their own node).

It provides the following modalities:

1. Each **content type** can have its own default content access
   settings by **role**.

2. Optionally you can enable role based access control settings per
   **node**.

3. Access control can be further customized per **user**.

For more information and reporting, please visit:

* For a description of the module, visit the [project page][1].
* For on-screen documentation, visit the [documentation page][2].
* To submit bug reports visit the project's [issue tracker][3].

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

To install, do the following:

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][4] for further information.
2. Enable the **ContentAccess** module on the *Modules* list
   page.

There is `composer.json` in the project's top directory.  You can
safely delete this.  This project has no third party dependencies. The
sole purpose of this file is to keep the Drupal.org **SimpleTest**
testbot happy.


## CONFIGURATION


### Role based access control per content type

To set up access control for a content type, navigate to **Manage »
Structure » Content types** and select "edit" for the content type you
want to set up.  After this module has been installed, there will be a
new tab named "Access Control" that let you control access.

To set up role based access control, tick the boxes under "Role based
access control settings".

### Per content node access control

There is a a checkbox to enable per content node access control
settings.  If enabled, a new tab for the content access settings
appears when viewing content.

To configure permission to access these settings, navigate to
*Administration » People » Permisions* and set the "Permit content
access" permission for the relevant roles.

### Using per user access control

To make set up per user access control you first need to enable per
node access control for the relevant content type. At the access
control tab of node of that content type you are now able to give
permission to view for specific users..

## NOTES

### Unpublished content

Note that a node that is not published is treated in a different way
by Drupal: It can be viewed only by its author if they have the
pernission "View own unpublished content", and by users that have the
permision "Bypass content access control".  You can *not* use this
project to manage access to unpublished content.

### About Drupal access control

A well-behaved Drupal node access control module can only permit
access.  It cannot deny it. If you are using multiple access control
modules, access will be given to a node as soon as one of the module
give access to it.


## MAINTAINER

**ContentAccess** was created by [gisle][5] (Gisle Hannemyr).

Development and maintenance is sponsored by [Hannemyr Nye Medier AS][6].

Any help with development (patches, reviews, comments) are welcome.

[1]: https://drupal.org/project/contentaccess
[2]: https://www.drupal.org/docs/contributed-modules/contentaccess
[3]: https://drupal.org/project/issues/contentaccess
[4]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules
[5]: https://www.drupal.org/u/gisle
[6]: https://hannemyr.no
